﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySqlConnector;
using System;

namespace DemoWebApi.Controllers
{
    [Route("demo")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        [HttpGet]
        public String Get()
        {
            /*          var mySqlConnection = new MySqlConnection("server=mysql;user=root;password=server;database=spring");
                        mySqlConnection.Open();
                        var mySqlCommand = new MySqlCommand("SELECT * FROM user;", mySqlConnection);
                        var mySqlReader = mySqlCommand.ExecuteReader();
                        var mySqlDatabases = "";

                        while (mySqlReader.Read())
                        {
                            mySqlDatabases += mySqlReader.GetString(0) + ",";
                        }

                        return "MySql Databases: " + mySqlDatabases; ;
            */
            return "Hello World";
        }

    }
}

